# coding: utf-8

import json

import pytest
from django.test import Client
from django.urls import reverse
from mixer.backend.django import mixer

from api.models import Device, Rule
from api.types import ProjectType


@pytest.fixture()
def devices(project):
    """5 devices fixture."""
    return mixer.cycle(5).blend(Device, project=project)


@pytest.fixture()
def device(project):
    """Devices fixture."""
    return mixer.blend(Device, project=project)


@pytest.fixture()
def rule(device):
    """Rule fixture."""
    return mixer.blend(Rule, rule_device=device)


@pytest.mark.django_db
class TestProjectDeviceRuleView:
    """Test for api.views.ProjectDeviceRuleView."""

    def test_get(self, user, project, device, rule):
        """Test project device rule get."""
        assert ProjectType.encode(
            project.project_type,
        ) == ProjectType.project_shared
        client = Client()
        response = client.get(
            reverse('project-device-rule', args=[
                project.slug,
                device.topic,
                rule.rule_class,
                rule.rule_call,
            ]),
            **{'HTTP_Authentication': device.api_key},
        )
        assert response.status_code == 200

    def test_get_unauth(
        self,
        user,
        project,
        device,
        rule,
    ):
        """Test project device rule get unauth."""
        client = Client()
        response = client.get(
            reverse('project-device-rule', args=[
                project.slug,
                device.topic,
                rule.rule_class,
                rule.rule_call,
            ]),
        )
        assert response.status_code == 403

    def test_get_non_exist_project(
        self,
        user,
        project,
        device,
        rule,
    ):
        """Test project device rule get, project not exist."""
        client = Client()
        non_exist_project = 'non-exist'
        if non_exist_project == project.slug:
            non_exist_project += '1'
        response = client.get(
            reverse('project-device-rule', args=[
                non_exist_project,
                device.topic,
                rule.rule_class,
                rule.rule_call,
            ]),
        )
        assert response.status_code == 404

    def test_get_non_exist_device(
        self,
        user,
        project,
        device,
        rule,
    ):
        """Test project device rule get, device not exist."""
        client = Client()
        non_exist_device = 'non-exist'
        if non_exist_device == device.topic:
            non_exist_device += '1'
        response = client.get(
            reverse('project-device-rule', args=[
                project.slug,
                non_exist_device,
                rule.rule_class,
                rule.rule_call,
            ]),
        )
        assert response.status_code == 404

    def test_get_non_exist_rule(
        self,
        user,
        project,
        device,
        rule,
    ):
        """Test project device rule get, rule not exist."""
        client = Client()
        non_exist_rule_class = 'non'
        non_exist_rule_call = 'exist'
        if non_exist_rule_class == rule.rule_class:
            non_exist_rule_class += '1'
        response = client.get(
            reverse('project-device-rule', args=[
                project.slug,
                device.topic,
                non_exist_rule_class,
                non_exist_rule_call,
            ]),
            **{'HTTP_Authentication': device.api_key},
        )
        assert response.status_code == 404


@pytest.mark.django_db
class TestProjectDeviceStatusViewGet:
    """Test api.views.ProjectDeviceStatusView get methods."""

    def test_get_unauth(self, project, device):
        """Test project device status get without token."""
        client = Client()

        response = client.get(
            reverse('project-device-status', args=[
                project.slug,
                device.topic,
            ]),
        )
        assert response.status_code == 403

    def test_get_code(self, project, device):
        """Test project device status get (code)."""
        client = Client()

        device.status = {'status': 0}
        device.save()

        response = client.get(
            reverse('project-device-status', args=[
                project.slug,
                device.topic,
            ]),
            **{'HTTP_Authentication': device.api_key},
        )
        assert response.status_code == 200

    def test_get_info(self, project, device):
        """Test project device status get (info)."""
        client = Client()

        device.status = {'status': 0}
        device.save()

        response = client.get(
            reverse('project-device-status', args=[
                project.slug,
                device.topic,
            ]),
            **{'HTTP_Authentication': device.api_key},
        )
        assert response.json() == device.status

    def test_get_same_project_device_code(self, project, devices):
        """Test project device status get (code)."""
        client = Client()

        devices[0].status = {'status': 0}
        devices[0].save()

        response = client.get(
            reverse('project-device-status', args=[
                project.slug,
                devices[0].topic,
            ]),
            **{'HTTP_Authentication': devices[1].api_key},
        )
        assert response.status_code == 200

    def test_get_same_project_device_wrong_code(
        self,
        project,
        devices,
    ):
        """Test project device status get (code)."""
        client = Client()

        devices[0].status = {'status': 0}
        devices[0].save()

        response = client.get(
            reverse('project-device-status', args=[
                project.slug,
                devices[0].topic,
            ]),
            **{'HTTP_Authentication': devices[1].api_key + '1'},
        )
        assert response.status_code == 403

    def test_get_project_not_exist(self, project, device):
        """Test non-exist project device status get (code)."""
        client = Client()

        device.status = {'status': 0}
        device.save()

        response = client.get(
            reverse('project-device-status', args=[
                project.slug + '1',
                device.topic,
            ]),
        )
        assert response.status_code == 404

    def test_get_device_not_exist(self, project, device):
        """Test project non-exist device status get (code)."""
        client = Client()

        device.status = {'status': 0}
        device.save()

        response = client.get(
            reverse('project-device-status', args=[
                project.slug,
                device.topic + '1',
            ]),
        )
        assert response.status_code == 404


@pytest.mark.django_db
class TestProjectDeviceStatusViewPost:
    """Test api.views.ProjectDeviceStatusView post methods."""

    def test_post_code(self, project, device):
        """Test project device status post (code)."""
        client = Client()

        status = {'status': 0}

        response = client.post(
            reverse('project-device-status', args=[
                project.slug,
                device.topic,
            ]),
            data={'status': json.dumps(status)},
            ** {'HTTP_Authentication': device.api_key},
        )
        assert response.status_code == 200

    def test_post_result(self, project, device):
        """Test project device status post (result)."""
        client = Client()

        status = {'status': 0}

        client.post(
            reverse('project-device-status', args=[
                project.slug,
                device.topic,
            ]),
            data={'status': json.dumps(status)},
            ** {'HTTP_Authentication': device.api_key},
        )
        device_new = Device.objects.get(pk=device.pk)

        assert device_new.status == status

    def test_post_same_project_device_code(self, project, devices):
        """Test same project device status post (code)."""
        client = Client()

        status = {'status': 0}

        response = client.post(
            reverse('project-device-status', args=[
                project.slug,
                devices[0].topic,
            ]),
            data={'status': json.dumps(status)},
            ** {'HTTP_Authentication': devices[1].api_key},
        )
        assert response.status_code == 403

    def test_post_project_not_exist(self, project, device):
        """Test non-exist project device status post (code)."""
        client = Client()

        status = {'status': 0}

        response = client.post(
            reverse('project-device-status', args=[
                project.slug + '1',
                device.topic,
            ]),
            data={'status': json.dumps(status)},
        )
        assert response.status_code == 404

    def test_post_device_not_exist(self, project, device):
        """Test project non-exist device status post (code)."""
        client = Client()

        status = {'status': 0}

        response = client.post(
            reverse('project-device-status', args=[
                project.slug,
                device.topic + '1',
            ]),
            data={'status': json.dumps(status)},
        )
        assert response.status_code == 404
