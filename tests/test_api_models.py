# coding: utf-8

"""Tests for api models."""

import pytest
from django.core.exceptions import ValidationError
from mixer.backend.django import mixer

from api.models import (
    DEVICE_KEY_LENGTH,
    PROJECT_KEY_LENGTH,
    PROJECT_TYPES,
    Device,
    Project,
    Rule,
)

mixer.register(
    'api.Project',
    project_type=PROJECT_TYPES[1][0],
    slug=None,
    api_key=None,
)


@pytest.fixture
def project_unsaved():
    """Unsaved project fixture."""
    project = Project(name='SoMe_Awesome-project #0')
    project.slug = project.generate_slug()
    return project


@pytest.fixture
def device_unsaved(project_unsaved):
    """Unsaved device fixture."""
    return Device(
        name='Microcontroller',
        topic='mkn0',
        project=project_unsaved,
    )


@pytest.fixture
def rule_unsaved(device_unsaved):
    """Unsaved device fixture."""
    return Rule(
        rule_device=device_unsaved,
        rule_class='event',
        rule_call='some',
    )


@pytest.mark.django_db
class TestApiModels:
    """Test for api models."""

    def test_project_save(self):
        """Test project creation."""
        old_count = Project.objects.count()
        mixer.blend('api.Project', slug=None, api_key=None)
        assert Project.objects.count() == old_count + 1


class TestApiModelsClasses:
    """Test for api models without db."""

    def test_project_slug(self, project_unsaved):
        """Test project slug autogeneration."""
        assert project_unsaved.generate_slug() == 'some-awesome-project-0'

    def test_project_str(self, project_unsaved):
        """Test project slug autogeneration."""
        assert str(project_unsaved) == 'some-awesome-project-0'

    def test_project_key(self, project_unsaved):
        """Test project key autogeneration."""
        assert len(project_unsaved.generate_key()) <= 2 * PROJECT_KEY_LENGTH

    def test_rule_str(self, rule_unsaved):
        """Device to string conversion must be 'project:device-class-call'."""
        assert str(rule_unsaved) == 'some-awesome-project-0:mkn0-event-some'


class TestApiDeviceClass:
    """Test for api device model without db."""

    def test_device_default_status(self):
        """Device default state must be empty dict."""
        device = Device(name='Microcontroller', topic='mkn0')
        assert device.status == {}

    def test_device_str(self, device_unsaved):
        """Device to string conversion must be 'topic-name'."""
        assert str(device_unsaved) == 'mkn0-Microcontroller'

    def test_device_key(self, device_unsaved):
        """Test project key autogeneration."""
        assert len(device_unsaved.generate_key()) <= 2 * DEVICE_KEY_LENGTH

    def test_invalid_device_topic(self, device_unsaved):
        """Test invalid device topic."""
        device_unsaved.topic = '$$$'
        with pytest.raises(ValidationError):
            device_unsaved.clean()
