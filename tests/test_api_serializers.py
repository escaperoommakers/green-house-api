# coding: utf-8

import pytest
from django.test import RequestFactory
from mixer.backend.django import mixer
from rest_framework.request import Request

from api.models import Device, Rule
from api.serializers import DeviceSerializer, RuleSerializer

mixer.register(
    'api.Device',
    status=dict,
)


@pytest.mark.django_db
class TestSerializers:
    """Test for serializers."""

    def test_device_serializer(self, project):
        """Test device serializer."""
        mixer.blend('api.Device', project=project)
        device = Device.objects.get(pk=1)
        serializer = DeviceSerializer(device)
        assert device.name == serializer.data['name']
        assert device.topic == serializer.data['topic']
        assert device.status == serializer.data['status']

    def test_rule_serializer(self, project):
        """Test rule serializer."""
        mixer.blend('api.Device', project=project)
        mixer.blend('api.Rule')
        rule = Rule.objects.get(pk=1)
        serializer = RuleSerializer(
            rule,
            context={'request': Request(RequestFactory().get('/'))},
        )
        assert rule.rule_class == serializer.data['class']
        assert rule.rule_call == serializer.data['call']
