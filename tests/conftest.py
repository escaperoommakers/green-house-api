# coding: utf-8

import pytest
from django.contrib.auth.models import User
from mixer.backend.django import mixer

from api.models import PROJECT_TYPES, Project
from tests import fakers


@pytest.fixture
def user():
    """User fixture."""
    return mixer.blend(User)


@pytest.fixture
def project(user):
    """Project fixture."""
    return mixer.blend(
        Project,
        project_type=PROJECT_TYPES[1][0],
        user=user,
        slug=mixer.MIX.name(fakers.get_project_slug),
    )
