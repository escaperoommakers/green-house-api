# coding: utf-8

"""Project related fakers."""

from api.models import Project


def get_project_slug(name):
    """Get slug for project."""
    return Project(name=name).generate_slug()
