# coding: utf-8

"""Set device status service."""

import json
from enum import Enum, auto

from attr import attrib, attrs
from stories import Failure, Success, arguments, story

from api.types import ProjectType


@attrs
class SetDeviceStatus:
    """Set device status."""

    @story
    @arguments(
        'project_slug',
        'device_topic',
        'auth_key',
        'new_status',
    )
    def run(self):
        """Set device status."""
        self.find_project  # noqa: Z444
        self.find_device  # noqa: Z444
        self.check_permissions  # noqa: Z444
        self.check_new_status  # noqa: Z444
        self.set_status  # noqa: Z444

    def find_project(self, context):
        """Find project by slug."""
        project = self.load_project(context.project_slug)
        if project:
            return Success(project=project)
        return Failure(reason=Errors.not_found)

    def find_device(self, context):
        """Find device by project and topic."""
        device = self.load_device(context.project, context.device_topic)
        if device:
            return Success(device=device)
        return Failure(reason=Errors.not_found)

    def check_permissions(self, context):
        """Check permission of device."""
        if context.project.project_type == ProjectType.project_shared:
            if context.device.api_key == context.auth_key:
                return Success()
        return Failure(reason=Errors.forbidden)

    def check_new_status(self, context):
        """Check new status correctance."""
        if context.new_status is None:
            return Failure(reason=Errors.status_incorrect)
        new_status_parsed = None
        try:
            new_status_parsed = json.loads(context.new_status)
        except ValueError:
            return Failure(reason=Errors)
        return Success(new_status_parsed=new_status_parsed)

    def set_status(self, context):
        """Set status to device."""
        self.set_device_status(context.device, context.new_status_parsed)
        return Success()

    # Dependencies

    load_project = attrib()
    load_device = attrib()
    set_device_status = attrib()

# Protocol definition.


@SetDeviceStatus.run.failures
class Errors(Enum):
    """Error codes."""

    not_found = auto()
    forbidden = auto()
    status_incorrect = auto()
