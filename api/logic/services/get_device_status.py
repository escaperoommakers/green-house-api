# coding: utf-8

"""Get device status service."""

from enum import Enum, auto

from attr import attrib, attrs
from stories import Failure, Result, Success, arguments, story

from api.types import ProjectType


@attrs
class GetDeviceStatus:
    """Get device status."""

    @story
    @arguments(
        'project_slug',
        'device_topic',
        'auth_key',
    )
    def run(self):
        """Get device status."""
        self.find_project  # noqa: Z444
        self.find_device  # noqa: Z444
        self.find_device_with_key  # noqa: Z444
        self.check_permissions  # noqa: Z444
        self.show_status  # noqa: Z444

    def find_project(self, context):
        """Find project by slug."""
        project = self.load_project(context.project_slug)
        if project:
            return Success(project=project)
        return Failure(Errors.not_found)

    def find_device(self, context):
        """Find device by project and topic."""
        device = self.load_device(context.project, context.device_topic)
        if device:
            return Success(device=device)
        return Failure(Errors.not_found)

    def find_device_with_key(self, context):
        """Find device with key."""
        device_with_key = self.load_device_by_api_key(context.auth_key)
        if device_with_key:
            return Success(device_with_key=device_with_key)
        return Failure(Errors.forbidden)

    def check_permissions(self, context):
        """Check permission of device."""
        if context.project.project_type == ProjectType.project_shared:
            if context.device.project == context.device_with_key.project:
                return Success()
        return Failure(Errors.forbidden)

    def show_status(self, context):
        """Return found device status."""
        return Result(context.device.status)

    # Dependencies.

    load_project = attrib()
    load_device = attrib()
    load_device_by_api_key = attrib()

# Protocol definition.


@GetDeviceStatus.run.failures
class Errors(Enum):
    """Error codes."""

    not_found = auto()
    forbidden = auto()
