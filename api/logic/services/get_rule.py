# coding: utf-8

"""Show rule service."""

from enum import Enum, auto

from attr import attrib, attrs
from stories import Failure, Result, Success, arguments, story

from api.types import ProjectType


@attrs
class GetRule:
    """Get rule."""

    @story
    @arguments(
        'project_slug',
        'device_topic',
        'auth_key',
        'rule_class',
        'rule_call',
    )
    def run(I):  # noqa: N803 N805 Z111
        """Get rule."""
        I.find_project  # noqa: Z444
        I.find_device  # noqa: Z444
        I.check_permissions  # noqa: Z444
        I.find_rule  # noqa: Z444
        I.show_rule  # noqa: Z444

    def find_project(self, context):
        """Find project by slug."""
        project = self.load_project(context.project_slug)
        if project is not None:
            return Success(project=project)
        return Failure(reason=Errors.not_found)

    def find_device(self, context):
        """Find device by project and topic."""
        device = self.load_device(context.project, context.device_topic)
        if device is not None:
            return Success(device=device)
        return Failure(reason=Errors.not_found)

    def check_permissions(self, context):
        """Check permission of device."""
        if context.project.project_type == ProjectType.project_shared:
            if context.device.api_key == context.auth_key:
                return Success()
        return Failure(reason=Errors.forbidden)

    def find_rule(self, context):
        """Find rule by device, class & call."""
        rule = self.load_rule(
            device=context.device,
            rule_class=context.rule_class,
            rule_call=context.rule_call,
        )
        if rule is not None:
            return Success(rule=rule)
        return Failure(reason=Errors.not_found)

    def show_rule(self, context):
        """Return found rule."""
        return Result(context.rule)

    # Dependencies

    load_project = attrib()
    load_device = attrib()
    load_rule = attrib()

# Protocol definition.


@GetRule.run.failures
class Errors(Enum):
    """Error codes."""

    not_found = auto()
    forbidden = auto()
