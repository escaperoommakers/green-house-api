# coding: utf-8

"""Rule admin register."""

from django.contrib import admin

from api.models import Rule

admin.site.register(Rule)
