# coding: utf-8

"""Device admin register."""

from django.contrib import admin

from api.models import Device

admin.site.register(Device)
