# coding: utf-8

"""Project admin register."""

from django.contrib import admin

from api.models import Project

admin.site.register(Project)
