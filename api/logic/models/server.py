# coding: utf-8

"""Go logic server model module."""

from django.db import models

from api.logic.models.constants import NAME_LENGTH


class Server(models.Model):
    """Go logic server model."""

    name = models.CharField(max_length=NAME_LENGTH)
