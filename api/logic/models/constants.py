# coding: utf-8

"""Constants for api."""

import re

PROJECT_SHARED_SSL = 0
PROJECT_SHARED = 1
PROJECT_PRIVATE = 2
PROJECT_PRIVATE_CACHE = 3

DEVICE_KEY_LENGTH = 20
PROJECT_KEY_LENGTH = 20
NAME_LENGTH = 200
TOPIC_LENGTH = 66536
IDENTIFER_LENGTH = 120

PROJECT_TYPES = [
    (PROJECT_SHARED_SSL, 'Shared MQTT SSL and Interpreter'),
    (PROJECT_SHARED, 'Shared MQTT and Interpreter'),
    (PROJECT_PRIVATE, 'Private MQTT and Interpreter'),
    (PROJECT_PRIVATE_CACHE, 'Private MQTT and Interpreter (with cache)'),
]
ACTUAL_PROJECT_TYPES = [
    PROJECT_TYPES[1],
]

TOPIC_VALIDATOR = re.compile("[0-9a-zA-Z-_.+!*'(),]+")
