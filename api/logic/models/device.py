# coding: utf-8

import binascii
import os

import jsonschema
from django.contrib.postgres.fields import JSONField
from django.core.exceptions import ValidationError
from django.db import models

from api.logic.models.constants import (
    DEVICE_KEY_LENGTH,
    NAME_LENGTH,
    TOPIC_LENGTH,
    TOPIC_VALIDATOR,
)
from api.logic.models.project import Project


class Device(models.Model):
    """Device of green house or quest."""

    name = models.CharField(max_length=NAME_LENGTH)
    topic = models.CharField(max_length=TOPIC_LENGTH, unique=True)
    status = JSONField(default=dict, blank=True)
    status_validator = JSONField(default=dict, blank=True)
    project = models.ForeignKey(Project, on_delete=models.CASCADE, null=True)
    api_key = models.CharField(
        blank=True,
        max_length=2 * DEVICE_KEY_LENGTH,
        null=True,
    )

    def generate_key(self):
        """Generate device API key."""
        return binascii.hexlify(os.urandom(DEVICE_KEY_LENGTH)).decode()

    def clean(self, *args, **kwargs):
        """Validate topic field."""
        if not TOPIC_VALIDATOR.fullmatch(self.topic):
            raise ValidationError(
                "Topic must contain only 0-9, a-z, A-Z, -_.+!*'(), ",
            )
        jsonschema.validate(self.status, self.status_validator)
        super().clean(*args, **kwargs)

    def save(self, *args, **kwargs):
        """Generate API key on create."""
        if not self.api_key:
            self.api_key = self.generate_key()
        return super().save(*args, **kwargs)

    def __str__(self):
        """Device visible name is 'topic-name'."""
        return '{0}-{1}'.format(self.topic, self.name)
