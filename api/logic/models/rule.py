# coding: utf-8

"""Rule for device action model module."""

from django.db import models

from api.logic.models.constants import IDENTIFER_LENGTH
from api.logic.models.device import Device


class Rule(models.Model):
    """Rule for device action model."""

    rule_device = models.ForeignKey(Device, on_delete=models.CASCADE)
    rule_class = models.CharField(max_length=IDENTIFER_LENGTH)
    rule_call = models.CharField(max_length=IDENTIFER_LENGTH)
    action = models.TextField(blank=True)

    class Meta:
        """Rules Django Meta class."""

        constraints = [
            models.constraints.UniqueConstraint(
                fields=['rule_device', 'rule_class', 'rule_call'],
                name='unique_rule',
            ),
        ]

    def __str__(self):
        """Device visible name is 'project:device-class-call'."""
        return '{0}:{1}-{2}-{3}'.format(
            self.rule_device.project.slug,
            self.rule_device.topic,
            self.rule_class,
            self.rule_call,
        )
