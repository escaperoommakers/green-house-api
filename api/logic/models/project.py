# coding: utf-8

"""Project auth space model module."""

import binascii
import os
import re

from django.contrib.auth.models import User
from django.db import models

from api.logic.models.constants import (
    ACTUAL_PROJECT_TYPES,
    NAME_LENGTH,
    PROJECT_KEY_LENGTH,
)


class Project(models.Model):
    """Project auth space."""

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=NAME_LENGTH)
    slug = models.SlugField(max_length=NAME_LENGTH, primary_key=True)
    project_type = models.SmallIntegerField(choices=ACTUAL_PROJECT_TYPES)
    api_key = models.CharField(max_length=2 * PROJECT_KEY_LENGTH)

    def generate_key(self):
        """Generate project api key."""
        return binascii.hexlify(os.urandom(PROJECT_KEY_LENGTH)).decode()

    def generate_slug(self):
        """Generate project slug from name."""
        slug = self.name.lower()
        slug = slug.replace(' ', '-')
        slug = slug.replace('_', '-')
        slug = re.sub('[^a-z0-9-]', '', slug)
        return slug

    def save(self, *args, **kwargs):
        """Generate API key and slug on create."""
        if not self.api_key:
            self.api_key = self.generate_key()
        if not self.slug:
            self.slug = self.generate_slug()
        return super().save(*args, **kwargs)

    def __str__(self):
        """Project visible name is slug."""
        return self.slug
