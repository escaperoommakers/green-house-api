# coding: utf-8

"""Repository of projects."""

from dataclasses import dataclass

from api.models import Project as ProjectModel
from api.types import ProjectType


@dataclass
class Project:
    """Project auth space dataclass."""

    pk: int
    user: int
    name: str
    slug: str
    project_type: ProjectType
    api_key: str


def load_project(project_slug: str) -> Project:
    """Load project."""
    try:
        project_model_object = ProjectModel.objects.get(slug=project_slug)
        return Project(
            pk=project_model_object.pk,
            user=project_model_object.user.pk,
            name=project_model_object.name,
            slug=project_slug,
            project_type=ProjectType.encode(project_model_object.project_type),
            api_key=project_model_object.api_key,
        )
    except ProjectModel.DoesNotExist:
        return None
