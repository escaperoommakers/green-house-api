# coding: utf-8

"""Repository of rules."""

from api.models import Rule


def load_rule(device, rule_class, rule_call):
    """Load device rule."""
    try:
        return Rule.objects.get(
            rule_device=device.pk,
            rule_class=rule_class,
            rule_call=rule_call,
        )
    except Rule.DoesNotExist:
        return None
