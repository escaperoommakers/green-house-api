# coding: utf-8

"""Repository of devices."""

from api.models import Device


def load_device(project, topic):
    """Load project device."""
    try:
        return Device.objects.get(
            project=project.pk,
            topic=topic,
        )
    except Device.DoesNotExist:
        return None


def load_device_by_api_key(api_key):
    """Load device by api key."""
    try:
        return Device.objects.get(
            api_key=api_key,
        )
    except Device.DoesNotExist:
        return None


def load_devices(project):
    """Load project devices."""
    return Device.objects.filter(
        project=project.pk,
    )


def set_device_status(device, status):
    """Set device status."""
    device.status = status
    device.save()
