# Generated by Django 2.2.1 on 2019-06-28 13:42

import django.contrib.postgres.fields.jsonb
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0004_auto_20190603_1640'),
    ]

    operations = [
        migrations.AddField(
            model_name='device',
            name='status_validator',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=dict),
        ),
    ]
