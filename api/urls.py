# coding: utf-8

"""api URL Configuration.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))

"""

from django.contrib import admin
from django.contrib.auth.views import LoginView, LogoutView
from django.urls import include, path
from rest_framework import routers

from api import views

router = routers.DefaultRouter()

apiv1 = [
    path(
        'project/{0}/device/{1}/rule/{2}/{3}/'.format(
            '<slug:project_slug>',
            '<device_topic>',
            '<rule_class>',
            '<rule_call>',
        ),
        views.ProjectDeviceRuleView.as_view(),
        name='project-device-rule',
    ),
    path(
        'project/<slug:project_slug>/device/<device_topic>/status/',
        views.ProjectDeviceStatusView.as_view(),
        name='project-device-status',
    ),
]

urlpatterns = [
    path('', include(router.urls)),
    path('api/v1/', include(apiv1)),
    path('admin/', admin.site.urls),
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),

]
