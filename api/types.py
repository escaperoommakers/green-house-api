# coding: utf-8

from enum import Enum, unique

from api.models import PROJECT_SHARED


@unique
class ProjectType(Enum):
    """Project type enumerator."""

    project_shared = PROJECT_SHARED

    @classmethod
    def encode(cls, number: int):
        """Encode number to enum."""
        if number == PROJECT_SHARED:
            return cls.project_shared
        raise ValueError('Incorrect project type.')
