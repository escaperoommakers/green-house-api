# coding: utf-8

import logging

from django.core.exceptions import PermissionDenied
from rest_framework.exceptions import NotFound, ParseError
from rest_framework.response import Response

from api.serializers import RuleSerializer
from dependencies import Injector, Package, operation, this
from dependencies.contrib.rest_framework import api_view

# Create your views here.

FORBIDDEN_CODE = 403
NOT_EXIST_CODE = 404
BAD_REQUEST = 400

LOGGER = logging.getLogger(__name__)

implemented = Package('api.implemented')


@api_view
class ProjectDeviceRuleView(Injector):
    """Allows specific rule for specific device to be viewed."""

    get_rule = implemented.GetRule.get_rule

    project_slug = this.kwargs['project_slug']
    device_topic = this.kwargs['device_topic']
    rule_class = this.kwargs['rule_class']
    rule_call = this.kwargs['rule_call']

    permission_classes = []

    @operation  # noqa: Z211
    def get(
        get_rule,  # noqa: N805
        project_slug,
        device_topic,
        rule_class,
        rule_call,
        request,
    ):
        """Get rule."""
        auth_key = request.headers.get('Authentication')
        result_container = get_rule.run(
            project_slug,
            device_topic,
            auth_key,
            rule_class,
            rule_call,
        )
        if result_container.is_success:
            serializer = RuleSerializer(
                result_container.value,
                context={'request': request},
            )
            return Response(serializer.data)
        if result_container.failed_because(get_rule.failures.not_found):
            raise NotFound(
                detail='Not found',
            )
        if result_container.failed_because(get_rule.failures.forbidden):
            raise PermissionDenied()


@api_view
class ProjectDeviceStatusView(Injector):
    """Allows specific device status to be viewed and edited."""

    get_status = implemented.GetDeviceStatus.get_status
    set_status = implemented.SetDeviceStatus.set_status

    project_slug = this.kwargs['project_slug']
    device_topic = this.kwargs['device_topic']

    permission_classes = []

    @operation
    def get(
        get_status,  # noqa: N805
        project_slug,
        device_topic,
        request,
    ):
        """Return device status."""
        auth_key = request.headers.get('Authentication')
        result_container = get_status.run(
            project_slug,
            device_topic,
            auth_key,
        )
        if result_container.is_success:
            return Response(result_container.value)
        if result_container.failed_because(get_status.failures.not_found):
            raise NotFound(
                detail='Not found',
            )
        if result_container.failed_because(get_status.failures.forbidden):
            raise PermissionDenied()

    @operation
    def post(set_status, project_slug, device_topic, request):  # noqa: N805
        """Set device status."""
        auth_key = request.headers.get('Authentication')
        new_status = request.POST.get('status', None)
        set_status_result = set_status.run(
            project_slug,
            device_topic,
            auth_key,
            new_status,
        )
        failures = set_status.failures
        if set_status_result.is_success:
            return Response({'status': 'ok'})
        if set_status_result.failed_because(failures.not_found):
            raise NotFound(
                detail='Not found',
            )
        if set_status_result.failed_because(failures.forbidden):
            raise PermissionDenied()
        if set_status_result.failed_because(failures.status_incorrect):
            raise ParseError(
                detail='Status incorrect',
            )
