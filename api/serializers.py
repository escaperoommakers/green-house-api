# coding: utf-8

from rest_framework import serializers

from api.models import Device, Rule


class DeviceSerializer(serializers.HyperlinkedModelSerializer):
    """Device serializer for Django REST Framework."""

    class Meta:
        """Device serializer meta."""

        model = Device
        fields = ('name', 'topic', 'status')


class RuleSerializer(serializers.HyperlinkedModelSerializer):
    """Rule serializer for Django REST Framework."""

    class Meta:
        """Rule serializer meta."""

        model = Rule
        fields = ('rule_class', 'rule_call', 'action')

    def to_representation(self, instance):
        """Return rule as dict with fields witout "rule_" prefix."""
        representation = super().to_representation(instance)
        representation['class'] = representation['rule_class']
        representation['call'] = representation['rule_call']
        representation.pop('rule_class')
        representation.pop('rule_call')
        return representation
