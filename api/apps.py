# coding: utf-8

"""Api application."""

from django.apps import AppConfig


class ApiConfig(AppConfig):
    """Api application config."""

    name = 'api'
