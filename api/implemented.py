# coding: utf-8

"""Story implementation."""
from dependencies import Injector, Package

services = Package('api.services')
repositories = Package('api.repositories')


class GetRule(Injector):
    """Implement get rule."""

    get_rule = services.GetRule.run
    load_project = repositories.load_project
    load_device = repositories.load_device
    load_rule = repositories.load_rule


class GetDeviceStatus(Injector):
    """Implement get device status."""

    get_status = services.GetDeviceStatus.run
    load_project = repositories.load_project
    load_device = repositories.load_device
    load_device_by_api_key = repositories.load_device_by_api_key


class SetDeviceStatus(Injector):
    """Implement set device status."""

    set_status = services.SetDeviceStatus.run
    load_project = repositories.load_project
    load_device = repositories.load_device
    set_device_status = repositories.set_device_status
